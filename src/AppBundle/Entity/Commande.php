<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commande
 *
 * @ORM\Table(name="commande")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommandeRepository")
 */
class Commande
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateOrder", type="datetime")
     */
    private $dateOrder;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateShipping", type="datetime")
     */
    private $dateShipping;

    /**
     * @ORM\ManyToMany(targetEntity="Clothe")
     */
    private $clothes;

    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="commandes")
     */
    private $client;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateOrder
     *
     * @param \DateTime $dateOrder
     *
     * @return Commande
     */
    public function setDateOrder($dateOrder)
    {
        $this->dateOrder = $dateOrder;

        return $this;
    }

    /**
     * Get dateOrder
     *
     * @return \DateTime
     */
    public function getDateOrder()
    {
        return $this->dateOrder;
    }

    /**
     * Set dateShipping
     *
     * @param \DateTime $dateShipping
     *
     * @return Commande
     */
    public function setDateShipping($dateShipping)
    {
        $this->dateShipping = $dateShipping;

        return $this;
    }

    /**
     * Get dateShipping
     *
     * @return \DateTime
     */
    public function getDateShipping()
    {
        return $this->dateShipping;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->clothes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add clothe
     *
     * @param \AppBundle\Entity\Clothe $clothe
     *
     * @return Commande
     */
    public function addClothe(\AppBundle\Entity\Clothe $clothe)
    {
        $this->clothes[] = $clothe;

        return $this;
    }

    /**
     * Remove clothe
     *
     * @param \AppBundle\Entity\Clothe $clothe
     */
    public function removeClothe(\AppBundle\Entity\Clothe $clothe)
    {
        $this->clothes->removeElement($clothe);
    }

    /**
     * Get clothe
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClothes()
    {
        return $this->clothes;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     *
     * @return Commande
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
